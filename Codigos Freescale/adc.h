#define promedio 100.00

AnalogIn acs712(A2);
AnalogIn lm35(A1);
AnalogIn vread(A3);
AnalogIn torq(A0);
AnalogIn emp(A4);

float current(float tara)
{
    float suma=0;
    int j, p=promedio; //p es el promedio
    
    //la tension de salida del sensor esta definida por read()*3.3=vcc/2+ 0.066*Ip
    //como el ADC entrega un valor porcentual, se multiplica por 3.3 para obtener una medida en tension
    //a la tension medida por el ADC se la multiplica por 2 para obtener la tension real, que fue atenuada por
    //un divisor resistivo a la mitad para que esta no se salga del rango de tension que maneja el ADC (0v-3.3v)
    
    for(j=0;j<p;j++)
    {
        suma = suma+ acs712.read();
    }
    
    suma = suma*3.3*2/p;
    suma = suma-tara;
    return suma/0.066; //0.066 es la sensibilidad del sensor: 0.066  V/A
}

float temp()
{
    float t=0; //voffset es la tension del lm35 a temperatura ambiente (25°C)
    int j=0;
    //la tension de salida del sensor esta definida por read()*3.3=0.01*T
    //como el ADC entrega un valor porcentual, se multiplica por 3.3 para obtener una medida en tension
    for(j=0;j<promedio;j++)
    {
        t += lm35.read()*3.3;
    }
    return t/(0.01*promedio); //0.01 es la sensibilidad del sensor: 0.01 V/°C
}

float read_tension()
{
    float tens=0; 
    int j=0;
    for(j=0;j<promedio;j++)
    {
        tens += vread.read()*3.3*(12.144/0.9236);
    }
    return tens/promedio;
}

float read_empuje(float tara)
{
    float tens=0; 
    int j=0;
    for(j=0;j<promedio;j++)
    {
        tens += emp.read()*3.3;
    } 
    tens=(tens/promedio)-tara;
    return tens;
}

float read_torque(float tara)
{
    float tens=0; 
    int j=0;
    for(j=0;j<promedio;j++)
    {
        tens += torq.read()*3.3;
    }
    tens=(tens/promedio)-tara;
    return tens;
}


float tara_corriente()
{
    int i;
    float suma=0;
    for(i=0;i<promedio;i++)
    {
        suma = suma + acs712.read();
    }
    return suma*3.3*2/promedio;
}

float tara_empuje()
{
    int i;
    float suma=0;
    for(i=0;i<promedio;i++)
    {
        suma = suma + emp.read();
    }
    return suma*3.3/promedio;
}

float tara_torque()
{
    int i;
    float suma=0;
    for(i=0;i<promedio;i++)
    {
        suma = suma + torq.read();
    }
    return suma*3.3/promedio;
}
