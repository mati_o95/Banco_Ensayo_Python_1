
Serial pc(USBTX, USBRX); // tx, rx


void envio(float empuje=0, float torque=0, float corriente=0,float tension=0,float temperatura=0, long rpm=0)
{
    //Defino las estructuras union
    union long_bytes { long l; char b[sizeof(long)];};
    union float_bytes { float f; char b[sizeof(float)];};
    //union int_bytes { int i; char b[sizeof(int)];};
    
    //Declaro las variables del tipo union
    //long_bytes Empuje, Torque;
    float_bytes Empuje,Torque;
    float_bytes Corriente;
    float_bytes Tension;
    float_bytes Temperatura;
    long_bytes Rpm;
    
    //Asigno valores 
    //Empuje.l = empuje;
    //Torque.l = torque;
    Empuje.f = empuje;
    Torque.f = torque;
    Corriente.f = corriente;
    Tension.f = tension;
    Temperatura.f = temperatura;
    Rpm.l = rpm;
    
    //Envio por puerto serie byte por byte
    //Empuje
    for (int i=0; i < 4; i++ ){
        pc.putc(Empuje.b[i]);
    }
    //Torque
    for (int i=0; i < 4; i++ ){
        pc.putc(Torque.b[i]);
    }
    //Corriente
    for (int i=0; i < 4; i++ ){
        pc.putc(Corriente.b[i]);
    }

    //Tension
    for (int i=0; i < 4; i++ ){
        pc.putc(Tension.b[i]);
    }

    //Temperatura
    for (int i=0; i < 4; i++ ){
        pc.putc(Temperatura.b[i]);
    }
    
    //Rpm
    for (int i=0; i < 4; i++ ){
        pc.putc(Rpm.b[i]);
    }
    
}

void recepcion(float *Ki,float *Kp, float *vref){
    char aux = 0;
    int i = 0;
    char dato[128] = {0};
    char comando = 0;
    do{
        aux = pc.getc();
        dato[i] = aux;
        i++;
    }while(aux != '\n');
    comando = dato[0];
    dato[0] = 48;
    
    if(comando=='A'){
        *vref=atof(dato)+500.0;
    }
    if(comando=='B'){
        *Ki=atof(dato);
    }
    if(comando=='C'){
        *Kp=atof(dato);
    }
}        

void set_baud(){
    pc.baud(115200);
}
 
