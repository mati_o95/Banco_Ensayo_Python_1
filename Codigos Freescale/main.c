#include "mbed.h"
//#include "hx711.h"
#include "envio.h"
#include "adc.h"

#define T_PWM   1000.0

void flip();

float acum_error = 0;
float Kp=1.0; 
float Ki=0.5;


//DigitalOut a3(A3);
DigitalOut a5(A5);
//DigitalOut a2(A2);
DigitalOut flash(LED2); //Led Rojo
DigitalOut led(LED1);  //Led Verde
InterruptIn in(PTA4);   //Conectado por jumper a PTB20, se cambio el A4 por D4/PTA4
//PwmOut pwm(PTE31);       //Asigno salida del PWM para control de llave H

Timer t1;
Ticker reset_pwm;
//Ticker recall_pi;



long v=0;
char a=0;
//float pwm_out=50;
float vref=500.0;
/*
int controlador(float v, float vref){
    float error;

    error = vref - v;
    if (pwm<1000){     //Pasar a DEFINE
        acum_error += error;   
    } 
    
    float Vout = Kp*error + Ki*acum_error;
    if(Vout>T_PWM/2)
        Vout = T_PWM/2;
    if(Vout<0)
        Vout = 0;
    int pwm = (int)(Vout + T_PWM/2);     
    if (pwm==1000){     //Pasar a DEFINE
        pwm=950;   
    }    
    return pwm;
}

void calculopwm (void){
        pwm_out=float(controlador((v/22.0)+500.0, vref))/float(1000);
        pwm.write(pwm_out);      // duty cycle, relative to period
        //pwm.write(0.70f);   
} */
void resetv (void){
        if (a){
            v=0;
        }
        a=1;
               
}

int main()
{
    float ta=0, tb=0; //Se usa para establecer la tara, ta= tara del canal a, tb= tara del canal b //Para hx son long
    float empuje=0, torque=0; //El empuje se lee del canal a (ganancia 128) y el torque del canal b (ganancia 32)
    float corriente=0, temperatura=0,tension=12.34, tc=0; //tc es la tara usada para el sensor de corriente

    //Pongo las entradas analogicas que no se usan como slidas digitales en 0 para que no introduzcan ruido
    //a3 = 0;
    a5 = 0;
    //a2 = 0;
    
    set_baud();
    
    wait(2);
    in.mode(PullNone);              // Set the pin to Pull Down mode.
    in.rise(&flip);                 // Set up the interrupt for rising edge
    t1.start();                     // start the timer
    
    //Para HX711
    //ta=tara(10,128);
    //tb=tara(10,32);
    //Para ADC
    ta=tara_empuje();
    tb=tara_torque(); 
    wait(2);
    
    tc=tara_corriente();
    
    //pwm.period_ms(1);      // 4 second period
    reset_pwm.attach(&resetv, 1.0);
    //recall_pi.attach(&calculopwm, 0.1);
    
    while(1){
        led=!led;
        //Para HX711
        //empuje=lectura(10,ta,128);
        //torque=lectura(10,tb,32);
        //Para ADC
        empuje=read_empuje(ta);
        torque=read_torque(tb);
        led=!led;
        corriente=current(tc);
        tension= read_tension();
        temperatura=temp();
        envio(empuje,torque,corriente,tension,temperatura,v);
        //wait(0.1);
        flash = !flash;
        
        if (pc.readable ()){
            recepcion(&Ki,&Kp,&vref);
        }
        wait_ms(50);          
    }
}

void flip() {
    int t_period = 0;
    a=0;
    
    t_period = t1.read_us();                // Lee el tiempo desde la ultima interrupcion
    v = (float)((60000000)/(t_period));     // Convierte el periodo (en us) a frecuencia (rpm)
    t1.reset();                             // Resetea el timer y espera a la proxima interrupcion
}
