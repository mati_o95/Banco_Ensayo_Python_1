#!/usr/bin/python
#Codigo en python para el banco de ensayo
#Importamos las librerias necesarias
import serial
import os
from struct import *
import datetime
import threading
import time
import xlwt
import sys
import signal
import numpy as np
import matplotlib.pyplot as plt

#Variables globales
factor_empuje = 1.0
factor_torque = 1.0
RPM_SET  = 0
x = 0

#--------------------------------------------------------------------------------------------------------
def Ensayo(Cantidad_muestras):
	#Consultamos la existencia de la carpeta "Ensayos/" si no existe la creamos
	if os.path.exists("Ensayos/")==False:
		os.system("mkdir Ensayos/")

	#Creamos hojas de excel y definimos estilos
	global x	
	x = datetime.datetime.now()
	style0 = xlwt.easyxf('font: name Times New Roman, colour red, bold on')
	style1 = xlwt.easyxf('',num_format_str='DD-MMM-YY')
	#wb = xlwt.Workbook() en variables globales
	ws = wb.add_sheet('A Test Sheet',cell_overwrite_ok=True)
	ws.write(0, 0, 'Prueba 1000 medidas', style0)
	ws.write(1, 0, x, style1)
	ws.write(1, 1, 'Tiempo')
	ws.write(1, 2, 'Empuje')
	ws.write(1, 3, 'Torque')
	ws.write(1, 4, 'Corriente')
	ws.write(1, 5, 'Tension')
	ws.write(1, 6, 'Temperatura')
	ws.write(1, 7, 'Rpm')	
	

	#Limpiamos el buffer de entrada del puerto serie
	Limpiar_puerto()

	#Creamos un buble
	i=1
	while (i <= Cantidad_muestras) or (Cantidad_muestras == 0):
		#leemos 22 bytes seguidos
		Rx = ''	
		while len(Rx)!= 22:
			try:
				#debug				
				Rx = PuertoSerie.read(22)
				#Rx = raw_input()
				#time.sleep(0.1)
				#Rx="1234567890123456789012"
			except:
				print "Puerto cerrado"
				exit(0)
		

		#separamos y convertimos los bytes en sus respectivos tipos de datos
		Empuje = unpack('i', Rx[0:4])		#4 byte -> long  (i)
		Torque = unpack('i', Rx[4:8])		#4 byte -> long  (i)
		Corriente = unpack('f', Rx[8:12])	#4 byte -> float (f)
		Tension = unpack('f', Rx[12:16])	#4 byte -> float (f)
		Temperatura = unpack('f', Rx[16:20])	#4 byte -> float (f)
		Rpm = unpack('h', Rx[20:22])		#2 byte -> int   (h)
							#22 bytes

		#Limpiamos la pantalla
		os.system('clear')

		# Mostramos los valores leidos en crudo
		print "Datos en crudo:"
		print "Empuje: " + str(Empuje[0])
		print "Torque: " + str(Torque[0])	
		print "Corriente: " + str(Corriente[0])
		print "Tension: " + str(Tension[0])
		print "Temperatura: " + str(Temperatura[0])
		print "Rpm: " + str(Rpm[0])

		#Procesamos los datos
		Empuje_Proc = float(Empuje[0])/(factor_empuje)
		Torque_Proc = float(Torque[0])/(factor_torque)
		Corriente_Proc = Corriente[0]		#Dato de corriente procesado en placa
		Tension_Proc = Tension[0]
		Temperatura_Proc = Temperatura[0]
		Rpm_Proc = Rpm[0]			#Dato de rpm procesado en placa
		
		#Calculamos el tiempo total transcurrido
		Tiempo_total = datetime.datetime.now() - x		

		#Mostramos los valores procesados
		print "\nDatos Procesados:"
		print "Empuje: " + str(Empuje_Proc)
		print "Torque: " + str(Torque_Proc)	
		print "Corriente: " + str(Corriente_Proc)
		print "Tension: " + str(Tension_Proc)
		print "Temperatura: " + str(Temperatura_Proc)
		print "Rpm: " + str(Rpm_Proc)
		print "\nTiempo [s]: " + str(Tiempo_total.total_seconds())
		print "Muestra " + str(i) + " de " + str(Cantidad_muestras)
		print "\nInserte Velocidad [0-10000rpm] ('q' para salir): " + str(RPM_SET)		

		#Graficamos
		
		ax1.scatter(Tiempo_total.total_seconds(), Empuje_Proc,color='y')
		#ax1.grid(True)
    		ax1.set_title('Empuje= %d[KG] Torque= %d[KG] Corriente= %d[A] RPM= %d[rpm]' %(Empuje_Proc,Torque_Proc,Corriente_Proc,Rpm_Proc),loc='left')

		   		
		ax2.scatter(Tiempo_total.total_seconds(), Torque_Proc,color='b') 
		#ax2.grid(True)  
    		ax3.scatter(Tiempo_total.total_seconds(), Corriente_Proc,color='r')
		#ax3.grid(True)    		
		ax4.scatter(Tiempo_total.total_seconds(), Rpm_Proc,color='g')
		#ax4.grid(True)
    		# Fine-tune figure; make subplots close to each other and hide x    ticks for
    		# all but bottom plot.
    		fig.subplots_adjust(hspace=0.005)
		  		
		plt.setp([a.get_xticklabels() for a in fig.axes[:-1]],  visible=False) 
		plt.xlim(Tiempo_total.total_seconds()-20,Tiempo_total.total_seconds())
		plt.draw()

		#Almacenamos datos en excel
		ws.write(i+1, 1, Tiempo_total.total_seconds())
		ws.write(i+1, 2, Empuje_Proc)
		ws.write(i+1, 3, Torque_Proc)
		ws.write(i+1, 4, Corriente_Proc)
		ws.write(i+1, 5, Tension_Proc)
		ws.write(i+1, 6, Temperatura_Proc)
		ws.write(i+1, 7, Rpm_Proc)
		i+=1		
		
	#guardo el archivo de excel

	wb.save('Ensayos/Ensayo_%s-%s-%s-%s-%s-%s.xls' %(x.year,x.month,x.day,x.hour,x.minute,x.second))

#--------------------------------------------------------------------------------------------------------
def Calibrar():
	cal_empuje = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
	cal_torque = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
	
	#Limpiamos el buffer de entrada del puerto serie
	Limpiar_puerto()
	
	#Tomo 10 lecturas calculo promedio, maximo, minimo y muestro los valores
	for i in range(0,10):	
		#leemos 22 bytes seguidos
		Rx = ''	
		while len(Rx)!= 22:
			try:
				#debug
				Rx = PuertoSerie.read(22)
				#Rx = raw_input()
				#time.sleep(0.1)
				#Rx="1234567890123456789012"
			except:
				print "Puerto cerrado"
				exit(0)
	
		#separamos y convertimos los bytes en sus respectivos tipos de datos
		cal_empuje[i] = unpack('i', Rx[0:4])		#4 byte -> long  (i)
		cal_torque[i] = unpack('i', Rx[4:8])		#4 byte -> long  (i)

		
	#calculos
	empuje_min = cal_empuje[0][0]
	empuje_pro = cal_empuje[0][0]
	empuje_max = cal_empuje[0][0]
	torque_min = cal_torque[0][0]
	torque_pro = cal_torque[0][0]
	torque_max = cal_torque[0][0]
	for i in range(1,10):
		if cal_empuje[i][0]<empuje_min:
			empuje_min = cal_empuje[i][0]
		if cal_empuje[i][0]>empuje_max:
			empuje_max = cal_empuje[i][0]
		if cal_torque[i][0]<torque_min:
			torque_min = cal_torque[i][0]
		if cal_torque[i][0]>torque_max:
			torque_max = cal_torque[i][0]
		empuje_pro += cal_empuje[i][0]
		torque_pro += cal_torque[i][0]
	empuje_pro = empuje_pro/10
	torque_pro = torque_pro/10
	
	#Muestro	
	os.system('clear')
	print "Empuje Minimo: " + str(empuje_min)
	print "Empuje Promedio: " + str(empuje_pro)
	print "Empuje Maximo: " + str(empuje_max)
	print "\nTorque Minimo: " + str(torque_min)
	print "Torque Promedio: " + str(torque_pro)
	print "Torque Maximo: " + str(torque_max)

	#pregunto el peso que se esta midiendo, calculo y modifico la variable del factor de escala
	global factor_empuje	#global por que quiero modificar las variables globales y no las locales
	global factor_torque	
	factor_empuje = input("\nEmpuje conocido[g]: ")
	factor_torque = input("Torque conocido[g]: ")
	factor_empuje = float(empuje_pro) / factor_empuje
	factor_torque = float(torque_pro) / factor_torque
	print "\nFactor de escala empuje: " + str(factor_empuje)
	print "Factor de escala torque: " + str(factor_torque)
	print ""

	#Vuelvo al menu principal
	Menu_principal()
#--------------------------------------------------------------------------------------------------------
def Menu_principal():
	#Muestro el menu principal
	#Capturo la opcion elejida y llamo a la funcion correspondiente
	ans = 0
	print("")
	while not(ans == "1" or ans == "2"):
		print("   1.Calibrar\n   2.Comenzar Ensayo")
		ans=raw_input("Seleccione una opcion [1-2]: ")
		if ans == "1":
			print("\nComenzando calibracion...") 
			Calibrar()
		elif ans == "2":
			print("\nComenzando Ensayo...")
			print("\nInserte cantidad de muestras: ")
			muestras = input() 
			#Iniciamos hilo paralelo, para la lectura del teclado
			thread_teclado = threading.Thread(target=Entrada_Teclado)
			thread_teclado.start()	
			Ensayo(muestras)	
		else:
			os.system('clear')			
			print("Opcion no valida") 
#--------------------------------------------------------------------------------------------------------
def Entrada_Teclado():
	Entrada = ""
	global RPM_SET
	while not(Entrada == "q") or (Entrada == "Q"):
      		#leo teclado
		Entrada = raw_input()
		if Entrada.isdigit():
			#Mando valor por puerto serie
			RPM_SET = Entrada
			#debug			
			PuertoSerie.write(str(RPM_SET))
	Salir()
#--------------------------------------------------------------------------------------------------------
def Salir():
	print("Guardando datos...")
	wb.save('Ensayos/Ensayo_%s-%s-%s-%s-%s-%s.xls' %(x.year,x.month,x.day,x.hour,x.minute,x.second))
	print("Cerrando puerto...")	
	#debug	
	PuertoSerie.close()
	exit(0)
#--------------------------------------------------------------------------------------------------------
def Limpiar_puerto():
	time.sleep(0.5) #espero 500ms
	#debug	
	while PuertoSerie.inWaiting():
		PuertoSerie.flushInput()
		time.sleep(0.001) #espero 1ms
	return 0
#--------------------------------------------------------------------------------------------------------
	


#INICIO del programa

#Abrimos el puerto serie a 9600, con un tiempo de espera de 1 segundos
#debug
PuertoSerie = serial.Serial('/dev/ttyACM0', 9600, timeout=1)

#Limpiamos el buffer de entrada del puerto serie
Limpiar_puerto()

#creo libro de excel
wb = xlwt.Workbook()

#Creamos la figura
fig,(ax1, ax2, ax3, ax4) = plt.subplots(4, sharex=True, sharey=True)
ax1.grid(True)
ax2.grid(True)
ax3.grid(True)
ax4.grid(True)


#llamo al menu principal
os.system('clear')
thread_menu = threading.Thread(target=Menu_principal)
thread_menu.start()	

plt.show()


