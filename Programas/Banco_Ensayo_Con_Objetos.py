#!/usr/bin/python
#Codigo en python para el banco de ensayo
#Importamos las librerias necesarias
import serial
import os
from struct import *
import datetime
import threading
import time
import xlwt
import sys
import signal
import numpy as np
import matplotlib.pyplot as plt


#--------------------------------------------------------------------------------------------------------------------------------------------
#Librarias:  Date, excel, os
class Archivo:
	def __init__(self, T = datetime.datetime.now()):
        	self.T = T

		#Consultamos la existencia de la carpeta "Ensayos/" si no existe la creamos
		if os.path.exists("Ensayos/")==False:
			os.system("mkdir Ensayos/")
	
		#Creamos hoja de excel, definimos estilos, y establecemos el formato de la tabla.
		style0 = xlwt.easyxf('font: name Times New Roman, colour red, bold on')
		style1 = xlwt.easyxf('',num_format_str='DD-MMM-YY')
		self.wb = xlwt.Workbook()
		self.ws = self.wb.add_sheet('A Test Sheet',cell_overwrite_ok=True)
		self.ws.write(0, 0, 'Ensayo', style0)
		self.ws.write(1, 0, T, style1)
		self.ws.write(1, 1, 'Tiempo')
		self.ws.write(1, 2, 'Empuje')
		self.ws.write(1, 3, 'Torque')
		self.ws.write(1, 4, 'Corriente')
		self.ws.write(1, 5, 'Tension')
		self.ws.write(1, 6, 'Temperatura')
		self.ws.write(1, 7, 'Rpm')

		#Numero de fila
		self.fila = 2	


	def Guardar(self,t,A,B,C,D,E,F):
		#Almacenamos datos en excel e incrementamos numero de fila
		self.ws.write(self.fila, 1, t)
		self.ws.write(self.fila, 2, A)
		self.ws.write(self.fila, 3, B)
		self.ws.write(self.fila, 4, C)
		self.ws.write(self.fila, 5, D)
		self.ws.write(self.fila, 6, E)
		self.ws.write(self.fila, 7, F)
		self.fila+=1		
		
	def __del__(self):
		#guardamos el archivo en disco
		print("Guardando datos...")
		self.wb.save('Ensayos/Ensayo_%s-%s-%s-%s-%s-%s.xls' %(self.T.year,self.T.month,self.T.day,self.T.hour,self.T.minute,self.T.second))



#--------------------------------------------------------------------------------------------------------------------------------------------
#Librarias: serial, time, date
class Puerto:
	def __init__(self,puerto = '/dev/ttyACM0'):
        	#Abrimos el puerto serie a 9600, con un tiempo de espera de 1 segundos
		self.PuertoSerie = serial.Serial(puerto, 9600, timeout=1)
		#Limpiamos el buffer de entrada del puerto serie
		self.Limpiar_puerto()	
		return

	def Limpiar_puerto(self):
		time.sleep(0.5) #espero 500ms	
		while self.PuertoSerie.inWaiting():
			self.PuertoSerie.flushInput()
			time.sleep(0.001) #espero 1ms
		return	

	def Rx(self):
		#leemos 22 bytes seguidos
		Trama = ''	
		while len(Trama)!= 22:
			try:
				Trama = self.PuertoSerie.read(22)
				#time.sleep(0.1)
				#Trama="1234567890123456789012"
			except:
				print "Puerto cerrado"
				exit(0)

		#separamos y convertimos los bytes en sus respectivos tipos de datos
		a = unpack('i', Trama[0:4])	#4 byte -> long  (i) Empuje
		b = unpack('i', Trama[4:8])	#4 byte -> long  (i) Torque
		c = unpack('f', Trama[8:12])	#4 byte -> float (f) Corriente
		d = unpack('f', Trama[12:16])	#4 byte -> float (f) Tension
		e = unpack('f', Trama[16:20])	#4 byte -> float (f) Temperatura
		f = unpack('h', Trama[20:22])	#2 byte -> int   (h) Rpm
						#22 bytes
		return (a,b,c,d,e,f)

		



	def Rx_Conversion (self,Factor_Empuje = 1, Factor_Torque = 1):
		a,b,c,d,e,f = self.Rx()

		#Procesamos los datos
		A = float(a[0])/(Factor_Empuje)
		B = float(b[0])/(Factor_Torque)
		C = c[0]
		D = d[0]
		E = e[0]
		F = f[0]
		
		return (A,B,C,D,E,F)

	def Calibrar(self,Empuje_medido, Torque_medido):
		cal_empuje = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
		cal_torque = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
		
		#Limpiamos el buffer de entrada del puerto serie
		self.Limpiar_puerto()		

		#Tomo 10 lecturas calculo promedio, maximo, minimo y muestro los valores
		for i in range(0,10):	
			a,b,c,d,e,f = self.Rx()
			cal_empuje[i] = a
			cal_torque[i] = b

		
		#calculos
		empuje_min = cal_empuje[0][0]
		empuje_pro = cal_empuje[0][0]
		empuje_max = cal_empuje[0][0]
		torque_min = cal_torque[0][0]
		torque_pro = cal_torque[0][0]
		torque_max = cal_torque[0][0]
		for i in range(1,10):
			if cal_empuje[i][0]<empuje_min:
				empuje_min = cal_empuje[i][0]
			if cal_empuje[i][0]>empuje_max:
				empuje_max = cal_empuje[i][0]
			if cal_torque[i][0]<torque_min:
				torque_min = cal_torque[i][0]
			if cal_torque[i][0]>torque_max:
				torque_max = cal_torque[i][0]
			empuje_pro += cal_empuje[i][0]
			torque_pro += cal_torque[i][0]
		empuje_pro = empuje_pro/10
		torque_pro = torque_pro/10

		#Calculo y modifico la variable del factor de escala
		Factor_Empuje = float(empuje_pro) / Empuje_medido
		Factor_Torque = float(torque_pro) / Torque_medido

		return (Factor_Empuje,Factor_Torque)

	def Enviar(self,dato):
		self.PuertoSerie.write(str(dato))
		return
	
	def __del__(self):
		print("Cerrando puerto...")	
		self.PuertoSerie.close()
		return


#--------------------------------------------------------------------------------------------------------
def Menu_principal():
	#Variables	
	ans = 0
	Factor_Empuje, Factor_Torque = 1,1
	Empuje, Torque, Corriente, Tension, Temperatura, Rpm = 0,0,0,0,0,0
	
	#Inicializo el puerto
	Port = Puerto('/dev/ttyACM0')		
	
	while not(ans == "q" or ans == "Q"):
		#Muetro el menu
		os.system('clear')			
		print("   1.Calibrar\n   2.Comenzar Ensayo\n   q.Salir")		
		ans=raw_input("Seleccione una opcion [1-2-q]: ")
		
		#Calibracion
		if ans == "1":
			print("\nComenzando calibracion...") 
			print("Inserte Empuje conocido: ")
			Empuje_conocido = input()
			print("Inserte Torque conocido: ")
			Empuje_torque = input()
			print("Tomando medidas...")
			Factor_Empuje, Factor_Torque = Port.Calibrar(Empuje_conocido,Empuje_torque)
		
		#Ensayo
		elif ans == "2":			
			#Creamos la figura
			fig,(ax1, ax2, ax3, ax4) = plt.subplots(4, sharex=True, sharey=True)
			plt.grid(True)	
			plt.show(block = False)			

			#Pido cantidad de muetras			
			print("\nComenzando Ensayo...")
			print("Inserte cantidad de muestras: ")
			Cantidad_muestras = input() 
			
			#Tomo hora de inicio, y creo el archivo			
			Tiempo_inicio = datetime.datetime.now()
			File = Archivo(Tiempo_inicio)
			
			#Limpiamos el buffer de entrada del puerto serie
			Port.Limpiar_puerto()				
			#Bucle
			i=1
			while (i <= Cantidad_muestras) or (Cantidad_muestras == 0):
				Empuje, Torque, Corriente, Tension, Temperatura, Rpm = Port.Rx_Conversion(Factor_Empuje, Factor_Torque)	
				Tiempo = datetime.datetime.now() - Tiempo_inicio
				#Imprimo datos
				os.system('clear')				
				print "\nDatos Procesados:"
				print "Empuje: " + str(Empuje)
				print "Torque: " + str(Torque)	
				print "Corriente: " + str(Corriente)
				print "Tension: " + str(Tension)
				print "Temperatura: " + str(Temperatura)
				print "Rpm: " + str(Rpm)
				print "\nTiempo [s]: " + str(Tiempo.total_seconds())
				print "Muestra " + str(i) + " de " + str(Cantidad_muestras)					
				#Agrego datos al archivo			
				File.Guardar(Tiempo.total_seconds(), Empuje, Torque, Corriente, Tension, Temperatura, Rpm)
				#Agrego datos a los graficos
				#GRAFICOS

				
				#Graficamos
				ax1.scatter(Tiempo.total_seconds(), Empuje, color = 'y')
    				ax1.set_title('Empuje= %d[KG] Torque= %d[KG] Corriente= %d[A] RPM= %d[rpm]' %(Empuje,Torque,Corriente,Rpm),loc='left')
    				ax2.scatter(Tiempo.total_seconds(), Torque, color = 'b')
    				ax3.scatter(Tiempo.total_seconds(), Corriente, color='r')
    				ax4.scatter(Tiempo.total_seconds(), Rpm, color='g')
    				# Fine-tune figure; make subplots close to each other and hide x    ticks for
    				# all but bottom plot.
    				fig.subplots_adjust(hspace=0.005)
    				plt.setp([a.get_xticklabels() for a in fig.axes[:-1]],  visible=False) 
				plt.xlim(Tiempo.total_seconds()-20,Tiempo.total_seconds())
				plt.draw()
	
				i+=1
			File.__del__()
	plt.close("")

#--------------------------------------------------------------------------------------------------------




#llamo al menu principal en un hilo
#thread_menu = threading.Thread(target=Menu_principal)
#thread_menu.start()	

Menu_principal()









