#!/usr/bin/python
#Codigo en python para el banco de ensayo
#Importamos las librerias necesarias
import serial
import os
from struct import *
import datetime
import threading
import time
import xlwt
import sys
import signal
import numpy as np
import matplotlib.pyplot as plt
from collections import deque


#--------------------------------------------------------------------------------------------------------------------------------------------
#Librarias:  Date, excel, os
class Archivo:
	def __init__(self, T = datetime.datetime.now()):
        	self.T = T

		#Consultamos la existencia de la carpeta "Ensayos/" si no existe la creamos
		if os.path.exists("Ensayos/")==False:
			os.system("mkdir Ensayos/")
	
		#Creamos hoja de excel, definimos estilos, y establecemos el formato de la tabla.
		style0 = xlwt.easyxf('font: name Times New Roman, colour red, bold on')
		style1 = xlwt.easyxf('',num_format_str='DD-MMM-YY')
		self.wb = xlwt.Workbook()
		self.ws = self.wb.add_sheet('A Test Sheet',cell_overwrite_ok=True)
		self.ws.write(0, 0, 'Ensayo', style0)
		self.ws.write(1, 0, T, style1)
		self.ws.write(1, 1, 'Tiempo')
		self.ws.write(1, 2, 'Empuje')
		self.ws.write(1, 3, 'Torque')
		self.ws.write(1, 4, 'Corriente')
		self.ws.write(1, 5, 'Tension')
		self.ws.write(1, 6, 'Temperatura')
		self.ws.write(1, 7, 'Rpm')

		#Numero de fila
		self.fila = 2	


	def Guardar(self,t,A,B,C,D,E,F):
		#Almacenamos datos en excel e incrementamos numero de fila
		self.ws.write(self.fila, 1, t)
		self.ws.write(self.fila, 2, A)
		self.ws.write(self.fila, 3, B)
		self.ws.write(self.fila, 4, C)
		self.ws.write(self.fila, 5, D)
		self.ws.write(self.fila, 6, E)
		self.ws.write(self.fila, 7, F)
		self.fila+=1		
		
	def __del__(self):
		#guardamos el archivo en disco
		print("Guardando datos...")
		self.wb.save('Ensayos/Ensayo_%s-%s-%s-%s-%s-%s.xls' %(self.T.year,self.T.month,self.T.day,self.T.hour,self.T.minute,self.T.second))



#--------------------------------------------------------------------------------------------------------------------------------------------
#Librarias: serial, time, date
class Puerto:
	def __init__(self,puerto = '/dev/ttyACM0'):
        	#Abrimos el puerto serie a 9600, con un tiempo de espera de 1 segundos
		self.PuertoSerie = serial.Serial(puerto, 115200, timeout=1)
		#Limpiamos el buffer de entrada del puerto serie
		self.Limpiar_puerto()	
		return

	def Limpiar_puerto(self):
		time.sleep(0.5) #espero 500ms	
		while self.PuertoSerie.inWaiting():
			self.PuertoSerie.flushInput()
			time.sleep(0.005) #espero 1ms
		return	

	def Rx(self):
		#leemos 24 bytes seguidos
		Trama = ''	
		while len(Trama)!= 24:
			try:
				Trama = self.PuertoSerie.read(24)
				#time.sleep(0.1)
				#Trama="1234567890123456789012"
			except:
				print "Puerto cerrado"
				exit(0)

		#separamos y convertimos los bytes en sus respectivos tipos de datos
		a = unpack('i', Trama[0:4])	#4 byte -> long  (i) Empuje
		b = unpack('i', Trama[4:8])	#4 byte -> long  (i) Torque
		c = unpack('f', Trama[8:12])	#4 byte -> float (f) Corriente
		d = unpack('f', Trama[12:16])	#4 byte -> float (f) Tension
		e = unpack('f', Trama[16:20])	#4 byte -> float (f) Temperatura
		f = unpack('i', Trama[20:24])	#4 byte -> int   (i) Rpm
						#24 bytes
		return (a,b,c,d,e,f)

		



	def Rx_Conversion (self,Factor_Empuje = 1, Factor_Torque = 1):
		a,b,c,d,e,f = self.Rx()

		#Procesamos los datos
		A = float(a[0])/(Factor_Empuje)
		B = float(b[0])/(Factor_Torque)
		C = c[0]
		D = d[0]
		E = e[0]
		F = f[0]
		
		return (A,B,C,D,E,F)

	def Calibrar(self,Empuje_medido, Torque_medido):
		cal_empuje = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
		cal_torque = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
		
		#Limpiamos el buffer de entrada del puerto serie
		self.Limpiar_puerto()		

		#Tomo 10 lecturas calculo promedio, maximo, minimo y muestro los valores
		for i in range(0,10):	
			a,b,c,d,e,f = self.Rx()
			cal_empuje[i] = a
			cal_torque[i] = b

		
		#calculos
		empuje_min = cal_empuje[0][0]
		empuje_pro = cal_empuje[0][0]
		empuje_max = cal_empuje[0][0]
		torque_min = cal_torque[0][0]
		torque_pro = cal_torque[0][0]
		torque_max = cal_torque[0][0]
		for i in range(1,10):
			if cal_empuje[i][0]<empuje_min:
				empuje_min = cal_empuje[i][0]
			if cal_empuje[i][0]>empuje_max:
				empuje_max = cal_empuje[i][0]
			if cal_torque[i][0]<torque_min:
				torque_min = cal_torque[i][0]
			if cal_torque[i][0]>torque_max:
				torque_max = cal_torque[i][0]
			empuje_pro += cal_empuje[i][0]
			torque_pro += cal_torque[i][0]
		empuje_pro = empuje_pro/10
		torque_pro = torque_pro/10

		#Calculo y modifico la variable del factor de escala
		if Empuje_medido != 0:		
			Factor_Empuje = float(empuje_pro) / Empuje_medido
		else:
			Factor_Empuje = 1
		if Torque_medido != 0:	
			Factor_Torque = float(torque_pro) / Torque_medido
		else:
			Factor_Torque = 1

		return (Factor_Empuje,Factor_Torque)

	def Enviar(self, dato):
		print dato		
		self.PuertoSerie.write(str(dato))	
		return
	
	def __del__(self):
		print("Cerrando puerto...")	
		self.PuertoSerie.close()
		return





#--------------------------------------------------------------------------------------------------------
from PyQt4 import QtGui  #contiene los widgets
class Ventana(QtGui.QWidget):
	Factor_Empuje = 1
	Factor_Torque = 1
	b_stop = 0
	def __init__(self):
		super(Ventana, self).__init__()
			
		#Set window tittle and size
		self.setWindowTitle("Banco de Ensayo")
		
		#Add a button
		self.btn_conectar = QtGui.QPushButton("Conectar", self)
		self.btn_conectar.clicked.connect(self.Conectar)
		self.btn_calibrar = QtGui.QPushButton("Calibrar", self)
		self.btn_calibrar.clicked.connect(self.Calibrar)
		self.btn_calibrar.setEnabled(False)
		self.btn_comenzar = QtGui.QPushButton("Comenzar", self)
		self.btn_comenzar.clicked.connect(self.Iniciar)
		self.btn_comenzar.setEnabled(False)
		self.btn_salir = QtGui.QPushButton("Salir", self)
		self.btn_salir.clicked.connect(self.Salir)
		self.btn_parar = QtGui.QPushButton("Stop", self)
		self.btn_parar.clicked.connect(self.Parar)
		self.btn_parar.setEnabled(False)
		self.btn_set_rpm = QtGui.QPushButton("Set RPM", self)
		self.btn_set_rpm.clicked.connect(self.Set_rpm)
		self.btn_set_rpm.setEnabled(False)
		self.btn_set_ki = QtGui.QPushButton("Set KI", self)
		self.btn_set_ki.clicked.connect(self.Set_ki)
		self.btn_set_ki.setEnabled(False)
		self.btn_set_kp = QtGui.QPushButton("Set KP", self)
		self.btn_set_kp.clicked.connect(self.Set_kp)
		self.btn_set_kp.setEnabled(False)


		#Labels
		label_1 = QtGui.QLabel("Puerto:",self)
		label_2 = QtGui.QLabel("Empuje conocido [g]:",self)
		label_3 = QtGui.QLabel("Torque conocido [g]:",self)
		label_4 = QtGui.QLabel("Factor de Empuje:",self)
		label_5 = QtGui.QLabel("Factor de Torque:",self)
		label_6 = QtGui.QLabel("Cantidad de muestras:",self)
		label_7 = QtGui.QLabel("Datos:",self)
		label_8 = QtGui.QLabel("Graficas:",self)
		label_9 = QtGui.QLabel("Empuje:",self)
		label_10 = QtGui.QLabel("Torque:",self)
		label_11 = QtGui.QLabel("Corriente:",self)
		label_12 = QtGui.QLabel("Tension:",self)
		label_13 = QtGui.QLabel("Temperatura:",self)
		label_14 = QtGui.QLabel("RPM:",self)
		label_15 = QtGui.QLabel("Tiempo:",self)
		label_16 = QtGui.QLabel("Nro Muestra:",self)
		label_17 = QtGui.QLabel("RPM:",self)
		label_18 = QtGui.QLabel("KI:",self)
		label_19 = QtGui.QLabel("KP:",self)
		self.label_fe = QtGui.QLabel(str(self.Factor_Empuje),self)
		self.label_ft = QtGui.QLabel(str(self.Factor_Torque),self)
		self.label_empuje = QtGui.QLabel("0",self)
		self.label_torque = QtGui.QLabel("0",self)
		self.label_corriente = QtGui.QLabel("0",self)
		self.label_tension = QtGui.QLabel("0",self)
		self.label_temperatura = QtGui.QLabel("0",self)
		self.label_rpm = QtGui.QLabel("0",self)
		self.label_tiempo = QtGui.QLabel("0",self)
		self.label_muestra = QtGui.QLabel("0",self)

		#Create textbox
		self.textbox_puerto = QtGui.QLineEdit("/dev/ttyACM0",self)
		self.textbox_empuje = QtGui.QLineEdit("0",self)
		self.textbox_torque = QtGui.QLineEdit("0",self)
		self.textbox_muestras = QtGui.QLineEdit("0",self)
		self.textbox_set_rpm = QtGui.QLineEdit("",self)
		self.textbox_set_ki = QtGui.QLineEdit("",self)
		self.textbox_set_kp = QtGui.QLineEdit("",self)
		
		#Checkboxs
		self.check_empuje = QtGui.QCheckBox("Empuje",self)
		self.check_torque = QtGui.QCheckBox("Torque",self)
		self.check_corriente = QtGui.QCheckBox("Corriente",self)
		self.check_tension = QtGui.QCheckBox("Tension",self)
		self.check_temperatura = QtGui.QCheckBox("Temperatura",self)
		self.check_rpm = QtGui.QCheckBox("RPM",self)
      		
		#Separadores
		Separador_1 = QtGui.QFrame()
		Separador_1.setFrameShape(QtGui.QFrame.HLine)
		Separador_1.setFrameShadow(QtGui.QFrame.Raised)
		Separador_1.setLineWidth (2)
		Separador_2 = QtGui.QFrame()
		Separador_2.setFrameShape(QtGui.QFrame.HLine)
		Separador_2.setFrameShadow(QtGui.QFrame.Raised)
		Separador_2.setLineWidth (2)

		#layouts
		grid = QtGui.QGridLayout()
		grid.setSpacing(10)

		grid.addWidget(self.btn_conectar, 1, 0)
		grid.addWidget(label_1, 1, 1)
		grid.addWidget(self.textbox_puerto, 1, 2)
		
		grid.addWidget(Separador_1, 2,0,1,3)		

		grid.addWidget(self.btn_calibrar, 3, 0)
		grid.addWidget(label_2, 3, 1)
		grid.addWidget(self.textbox_empuje, 3, 2)
		
		grid.addWidget(label_4, 4, 1)
		grid.addWidget(self.label_fe, 4, 2)

		grid.addWidget(label_3, 5, 1)
		grid.addWidget(self.textbox_torque, 5, 2)

		grid.addWidget(label_5, 6, 1)
		grid.addWidget(self.label_ft, 6, 2)

		grid.addWidget(Separador_2, 7,0,1,3)
		
		grid.addWidget(self.btn_comenzar, 8, 0)
		grid.addWidget(label_6, 8, 1)
		grid.addWidget(self.textbox_muestras, 8, 2)
		
		grid.addWidget(label_7, 10, 0)
		grid.addWidget(label_8, 10, 2)

		grid.addWidget(label_9, 11, 0)
		grid.addWidget(self.label_empuje, 11, 1)
		grid.addWidget(self.check_empuje, 11, 2)
		
		grid.addWidget(label_10, 12, 0)
		grid.addWidget(self.label_torque, 12, 1)
		grid.addWidget(self.check_torque, 12, 2)

		grid.addWidget(label_11, 13, 0)
		grid.addWidget(self.label_corriente, 13, 1)
		grid.addWidget(self.check_corriente, 13, 2)

		grid.addWidget(label_12, 14, 0)
		grid.addWidget(self.label_tension, 14, 1)
		grid.addWidget(self.check_tension, 14, 2)

		grid.addWidget(label_13, 15, 0)
		grid.addWidget(self.label_temperatura, 15, 1)
		grid.addWidget(self.check_temperatura, 15, 2)

		grid.addWidget(label_14, 16, 0)
		grid.addWidget(self.label_rpm, 16, 1)
		grid.addWidget(self.check_rpm, 16, 2)

		grid.addWidget(label_15, 17, 0)
		grid.addWidget(self.label_tiempo, 17, 1)

		grid.addWidget(label_16, 18, 0)
		grid.addWidget(self.label_muestra, 18, 1)
		grid.addWidget(self.btn_parar, 18, 2)

		grid.addWidget(label_17, 3, 3)	
		grid.addWidget(self.textbox_set_rpm, 3, 4)	
		grid.addWidget(self.btn_set_rpm, 3, 5)

		grid.addWidget(label_18, 4, 3)	
		grid.addWidget(self.textbox_set_ki, 4, 4)	
		grid.addWidget(self.btn_set_ki, 4, 5)

		grid.addWidget(label_19, 5, 3)
		grid.addWidget(self.textbox_set_kp, 5, 4)	
		grid.addWidget(self.btn_set_kp, 5, 5)
			
		grid.addWidget(self.btn_salir, 22, 2)	
				
		self.setLayout(grid)

	#Metodos
	def Conectar(self):
		try:
			print "Conectando..."
			self.Port = Puerto(self.textbox_puerto.text())
			#self.Port = Puerto("/dev/ttyACM0")
			self.textbox_puerto.setEnabled(False)	
			self.btn_conectar.setEnabled(False)
			self.btn_calibrar.setEnabled(True)
			self.btn_comenzar.setEnabled(True)
			self.btn_set_rpm.setEnabled(True)
			self.btn_set_ki.setEnabled(True)
			self.btn_set_kp.setEnabled(True)
			print "Conectado"
		except:
			print "Puerto cerrado"

	def Calibrar(self):
		print "Calibrando..."		
		self.btn_calibrar.setEnabled(False)
		self.btn_comenzar.setEnabled(False)		
		self.textbox_empuje.setEnabled(False)
		self.textbox_torque.setEnabled(False)
		self.btn_set_rpm.setEnabled(False)
		self.btn_set_ki.setEnabled(False)
		self.btn_set_kp.setEnabled(False)		
		self.Factor_Empuje, self.Factor_Torque = self.Port.Calibrar(float(self.textbox_empuje.text()),float(self.textbox_torque.text()))
		self.label_fe.setText(str(self.Factor_Empuje))
		self.label_ft.setText(str(self.Factor_Torque))
		self.textbox_empuje.setEnabled(True)
		self.textbox_torque.setEnabled(True)	
		self.btn_calibrar.setEnabled(True)
		self.btn_comenzar.setEnabled(True)
		self.btn_set_rpm.setEnabled(True)
		self.btn_set_ki.setEnabled(True)
		self.btn_set_kp.setEnabled(True)
		print "Calibrado"		
	
	def Iniciar(self):	
		self.b_stop = 0		
		self.thread_ensayo = threading.Thread(target= self.Comenzar)
		self.thread_ensayo.start()

	def Comenzar(self):
		
		print "Comenzando..."	
		self.btn_calibrar.setEnabled(False)
		self.btn_comenzar.setEnabled(False)
		self.btn_parar.setEnabled(True)
		self.check_empuje.setEnabled(False)
		self.check_torque.setEnabled(False)
		self.check_corriente.setEnabled(False)
		self.check_tension.setEnabled(False)
		self.check_temperatura.setEnabled(False)
		self.check_rpm.setEnabled(False)
	


		#Creamos la figura segun checkboxs
		#Cuento la cantidad de checkbox activos para saber en cuando dividio la fig
		posicion = 11		
		if self.check_empuje.isChecked() == True:
			posicion +=100
		if self.check_torque.isChecked() == True:
			posicion +=100
		if self.check_corriente.isChecked() == True:
			posicion +=100	
		if self.check_tension.isChecked() == True:
			posicion +=100
		if self.check_temperatura.isChecked() == True:
			posicion +=100
		if self.check_rpm.isChecked() == True:
			posicion +=100
		
		fig = plt.figure(figsize=(10,10))
		if self.check_empuje.isChecked() == True:		
			ax_empuje = fig.add_subplot(posicion)
			plt.ylabel('Empuje')		
			plt.grid(True)
			posicion +=1
		if self.check_torque.isChecked() == True:		
			ax_torque = fig.add_subplot(posicion)
			plt.ylabel('Torque')
			plt.grid(True)
			posicion +=1
		if self.check_corriente.isChecked() == True:
			ax_corriente =fig.add_subplot(posicion)
			plt.ylabel('Corriente')
			plt.grid(True)	
			posicion +=1	
		if self.check_tension.isChecked() == True:
			ax_tension = fig.add_subplot(posicion)
			plt.ylabel('Tension')
			plt.grid(True)
			posicion +=1
		if self.check_temperatura.isChecked() == True:
			ax_temperatura = fig.add_subplot(posicion)
			plt.ylabel('Temperatura')
			plt.grid(True)	
			posicion +=1	
		if self.check_rpm.isChecked() == True:
			ax_rpm = fig.add_subplot(posicion)
			plt.ylabel('Rpm')
			plt.grid(True)	
			posicion +=1
		#fig.subplots_adjust(hspace=0.01)
		plt.setp([a.get_xticklabels() for a in fig.axes[:-1]],  visible=False) 
		plt.show(block = False)				


		#Pido cantidad de muetras			
		Cantidad_muestras = int(self.textbox_muestras.text())
		
		#Tomo hora de inicio, y creo el archivo			
		Tiempo_inicio = datetime.datetime.now()
		File = Archivo(Tiempo_inicio)
		
		#Limpiamos el buffer de entrada del puerto serie
		self.Port.Limpiar_puerto()				
		
		#Bucle
		i=1
		j=0
		Time_Vect=[0,0]
		Emp_Vect=[0,0]
		Torq_Vect=[0,0]
		Corr_Vect=[0,0]
		Tens_Vect=[0,0]
		Temp_Vect=[0,0]
		Rpm_Vect=[0,0]
		emp_max= deque()
		torq_ma= deque()
		corr_max=deque()
		tens_max=deque()
		temp_max=deque()
		rpm_max=deque()

		while ((i <= Cantidad_muestras) or (Cantidad_muestras == 0))and self.b_stop == 0:
			Empuje, Torque, Corriente, Tension, Temperatura, Rpm = self.Port.Rx_Conversion(self.Factor_Empuje, self.Factor_Torque)	
			Tiempo = datetime.datetime.now() - Tiempo_inicio
			
			#Imprimo datos	
			self.label_empuje.setText(str(Empuje))
			self.label_torque.setText(str(Torque))
			self.label_corriente.setText(str(Corriente))
			self.label_tension.setText(str(Tension))
			self.label_temperatura.setText(str(Temperatura))
			self.label_rpm.setText(str(Rpm))
			self.label_tiempo.setText(str(Tiempo))
			self.label_muestra.setText(str(i) + " de " + str(Cantidad_muestras))
		
			#Agrego datos al archivo			
			File.Guardar(Tiempo.total_seconds(), Empuje, Torque, Corriente, Tension, Temperatura, Rpm)
			
			##Agrego bucle para que representa cada 10 datos -> Para no perder tiempo real, cada 10 paquetes de datos que llegan
			##actualizo el grafico
			
			Time_Vect.insert(j,Tiempo.total_seconds())
			Emp_Vect.insert(j,Empuje)
			Torq_Vect.insert(j,Torque)
			Corr_Vect.insert(j,Corriente)
			Tens_Vect.insert(j,Tension)
			Temp_Vect.insert(j,Temperatura)
			Rpm_Vect.insert(j,Rpm)
			j+=1
			


			
			if(j==10):
				
                #Agrego datos a los graficos y modificamos escalas, segun checkbox				
				if self.check_empuje.isChecked() == True:			
					plt.sca(ax_empuje)
					if (Time_Vect[9]>=20):
						for l in range(9):
							emp_max.popleft()
					for l in range(10):				
						emp_max.append(Emp_Vect[l])
					plt.ylim(min(emp_max),max(emp_max))
					plt.xlim(Time_Vect[9]-20,Time_Vect[9])
					ax_empuje.scatter(Time_Vect, Emp_Vect, color = 'y')
				if self.check_torque.isChecked() == True:
					if (Time_Vect[9]>=20):
						for l in range(9):
							torq_max.popleft()
					for l in range(10):			
						torq_max.append(Torq_Vect[l])
					plt.sca(ax_torque)
					plt.ylim(min(torq_max),max(torq_max))
					plt.xlim(Time_Vect[9]-20,Time_Vect[9])
					ax_torque.scatter(Time_Vect, Torq_Vect, color = 'b') 
				if self.check_corriente.isChecked() == True:
					if (Time_Vect[9]>=20):
						for l in range(9):
							corr_max.popleft()
					for l in range(10):
						corr_max.append(Corr_Vect[l]) 
					plt.sca(ax_corriente)					
					plt.ylim(min(corr_max),max(corr_max))
					plt.xlim(Time_Vect[9]-20,Time_Vect[9])
					ax_corriente.scatter(Time_Vect, Corr_Vect, color='r')
				if self.check_tension.isChecked() == True:
					if (Time_Vect[9]>=20):
						for l in range(9):
							tens_max.popleft()
					for l in range(10):				
						tens_max.append(Tens_Vect[l])
					plt.sca(ax_tension)
					plt.ylim(min(tens_max),max(tens_max))
					plt.xlim(Time_Vect[9]-20,Time_Vect[9])
					ax_tension.scatter(Time_Vect, Tens_Vect, color='g')
				if self.check_temperatura.isChecked() == True:
					if (Time_Vect[9]>=20):
						for l in range(9):
							temp_max.popleft()
					for l in range(10):
						temp_max.append(Temp_Vect[l])
					plt.sca(ax_temperatura)	
					plt.ylim(min(temp_max),max(temp_max))
					plt.xlim(Time_Vect[9]-20,Time_Vect[9])
					ax_temperatura.scatter(Time_Vect, Temp_Vect, color='k')
				if self.check_rpm.isChecked() == True:
					if (Time_Vect[9]>=20):
						for l in range(9):
							rpm_max.popleft()
					for l in range(10):
						rpm_max.append(Rpm_Vect[l])
					plt.sca(ax_rpm)
					plt.ylim(min(rpm_max),max(rpm_max))
					plt.xlim(Time_Vect[9]-20,Time_Vect[9])
					ax_rpm.scatter(Time_Vect, Rpm_Vect, color='b')    	
				plt.draw()
				j=0;
	

			i+=1

		plt.close('all')		
		File.__del__()

		self.btn_calibrar.setEnabled(True)
		self.btn_comenzar.setEnabled(True)
		self.btn_parar.setEnabled(False)
		self.check_empuje.setEnabled(True)
		self.check_torque.setEnabled(True)
		self.check_corriente.setEnabled(True)
		self.check_tension.setEnabled(True)
		self.check_temperatura.setEnabled(True)
		self.check_rpm.setEnabled(True)	

	def Set_rpm(self):
		self.Port.Enviar('A')
		time.sleep(0.5)		
		self.Port.Enviar(self.textbox_set_rpm.text())
		time.sleep(0.5)		
		self.Port.Enviar('\n')
	
	def Set_ki(self):
		self.Port.Enviar('B')
		time.sleep(0.5)	
		self.Port.Enviar(self.textbox_set_ki.text())
		time.sleep(0.5)		
		self.Port.Enviar('\n')

	def Set_kp(self):
		self.Port.Enviar('C')
		time.sleep(0.5)	
		self.Port.Enviar(self.textbox_set_kp.text())
		time.sleep(0.5)		
		self.Port.Enviar('\n')

	def Parar(self):
		self.b_stop = 1
			
	def Salir(self):
		self.b_stop = 1	
		exit()
		

#Inicio con GUI------------------------------------------------------------------------------------------
app = QtGui.QApplication(sys.argv)
w = Ventana()
w.show()
#mainloop -> mantiene viva a la aplicacion
sys.exit(app.exec_())


#Inicio en consola---------------------------------------------------------------------------------------
#Menu_principal()









