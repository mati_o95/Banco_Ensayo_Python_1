import matplotlib.pyplot as plt
import numpy as np
import time 

#x = np.arange(0, 2*np.pi, 0.01)
x = [0,1,2,3,4,5,6,7,8,9]
#y = np.sin(x[0])
y = [0,1,2,3,4,5,6,7,8,9]
n=x
m=y

a=[10,11,12,13,14,15,16,17,18,19]
b=[20,21,22,23,24,25,26,27,28,29]
c=[30,31,32,33,34,35,36,37,38,39]
d=[40,41,42,43,44,45,46,47,48,49]
e=[50,51,52,53,54,55,56,57,58,59]
f=[60,61,62,63,64,65,66,67,68,69]
t=[a,b,c,d,e,f]
#y=[5,7,8,9,6,4,7,4,6,9,6]
p=[4,6,8,3,1,6,7,9,5,4]
q=[66,78,66,2,3,44,3,6,7,8]
r=[0,2,3,5,9,3,14,22,36,34]
u=[8,9,7,6,6,3,4,8,2,5]
v=[1,1,3,7,20,6,30,4,5,4]
w=[2,4,3,5,3,6,3,8,9,4]
z=[10,14,15,16,2,3,48,52,3,2]

l=[u,v,w,z]

fig, (ax1) = plt.subplots(nrows=1)
fig.show()
#ax1.set_autoscale_on(True) # enable autoscale
#ax1.autoscale_view(True,True,True)

fig.canvas.draw()

line = ax1.plot(x, y, 'b-', animated=True)[0]


background1 = fig.canvas.copy_from_bbox(ax1.bbox)


tstart = time.time()
for j in range(0,6):
	for i in range(0, 9):
		fig.canvas.restore_region(background1)
		#y.append(l[j][i])
		m.append(l[j][i])
		n.append(t[j][i])
		line.set_ydata(m)
		line.set_xdata(n)
		#line.set_ydata(y.append(l[j][i]))
		#line.set_xdata(x.append(t[j][i]))
		#print y
		#print x
		#ax1.set_xlim(0, max(x))
		ax1.relim()        # Recalculate limits
		ax1.autoscale_view(True,True,True) #Autoscale
		ax1.draw_artist(line)
		fig.canvas.blit(ax1.bbox)
		plt.pause(0.01)


print 'FPS:' , 4/(time.time()-tstart)
