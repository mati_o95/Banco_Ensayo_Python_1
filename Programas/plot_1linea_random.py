import matplotlib.pyplot as plt
import numpy as np
import time
import random 

#x = np.arange(0, 2*np.pi, 0.01)
x = [0,1,2,3,4,5,6,7,8,9]
#y = np.sin(x[0])
y = [0,1,2,3,4,5,6,7,8,9]
n=x
m=y

t=[]
l=[]

#Genero Vector de Tiempos
for i in range(10,1010):
	t.append(i)

#Genero Vector Amplitud random
for i in range(0,1000):
    l.append(random.randint(0, 520))




fig, (ax1) = plt.subplots(nrows=1)
fig.show()
#ax1.set_autoscale_on(True) # enable autoscale
#ax1.autoscale_view(True,True,True)

fig.canvas.draw()

line = ax1.plot(x, y, 'b-', animated=True)[0]


background1 = fig.canvas.copy_from_bbox(ax1.bbox)


tstart = time.time()
for i in range(0, 1000):
	fig.canvas.restore_region(background1)
	#y.append(l[j][i])
	m.append(l[i])
	n.append(t[i])
	line.set_ydata(m)
	line.set_xdata(n)
	#line.set_ydata(y.append(l[j][i]))
	#line.set_xdata(x.append(t[j][i]))
	#print y
	#print x
	#ax1.set_xlim(0, max(x))
	ax1.relim()        # Recalculate limits
	ax1.autoscale_view(True,True,True) #Autoscale
	ax1.draw_artist(line)
	fig.canvas.blit(ax1.bbox)
	plt.pause(0.00001)


print 'FPS:' , 4/(time.time()-tstart)
