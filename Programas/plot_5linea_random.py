import matplotlib.pyplot as plt
import numpy as np
import time
import random 

#x = np.arange(0, 2*np.pi, 0.01)
x = [0,1,2,3,4,5,6,7,8,9]
#y = np.sin(x[0])
y = [0,1,2,3,4,5,6,7,8,9]
n=x
m=y

t=[]
l=[]

#Genero Vector de Tiempos
for i in range(10,1010):
	t.append(i)

#Genero Vector Amplitud random
for i in range(0,1000):
    l.append(random.randint(0, 520))
	#o.append(random.randint(0, 520))
	#p.append(random.randint(0, 520))
	#q.append(random.randint(0, 520))
	#r.append(random.randint(0, 520))
	#s.append(random.randint(0, 520))




fig, (ax1,ax2,ax3,ax4,ax5,ax6) = plt.subplots(nrows=6)
fig.show()

fig.canvas.draw()

line = ax1.plot(x, y, 'b-', animated=True)[0]
line2 = ax2.plot(x, y, 'r-', animated=True)[0]
line3= ax3.plot(x, y, 'g-', animated=True)[0]
line4= ax4.plot(x, y, 'c-', animated=True)[0]
line5= ax5.plot(x, y, 'm-', animated=True)[0]
line6= ax6.plot(x, y, 'y-', animated=True)[0]

ax1.set_xlim(0, 12)
ax2.set_xlim(4, 20)
background1 = fig.canvas.copy_from_bbox(ax1.bbox)

background2 = fig.canvas.copy_from_bbox(ax2.bbox)
background3 = fig.canvas.copy_from_bbox(ax3.bbox)
background4 = fig.canvas.copy_from_bbox(ax4.bbox)
background5 = fig.canvas.copy_from_bbox(ax5.bbox)
background6 = fig.canvas.copy_from_bbox(ax6.bbox)

tstart = time.time()
for i in range(0, 1000):
	fig.canvas.restore_region(background1)
	fig.canvas.restore_region(background2)
	fig.canvas.restore_region(background3)
	fig.canvas.restore_region(background4)
	fig.canvas.restore_region(background5)
	fig.canvas.restore_region(background6)
	#y.append(l[j][i])
	m.append(l[i])
	n.append(t[i])
	line.set_ydata(m)
	line.set_xdata(n)
	#line.set_ydata(y.append(l[j][i]))
	#line.set_xdata(x.append(t[j][i]))
	#print y
	#print x
	#ax1.set_xlim(0, max(x))
	ax1.relim()        # Recalculate limits
	ax1.autoscale_view(True,True,True) #Autoscale
	ax1.draw_artist(line)

	line2.set_ydata(m)
	line2.set_xdata(n)
	ax2.relim()        # Recalculate limits
	ax2.autoscale_view(True,True,True) #Autoscale
	ax2.draw_artist(line2)


	line3.set_ydata(m)
	line3.set_xdata(n)    
	ax3.relim()        # Recalculate limits
	ax3.autoscale_view(True,True,True) #Autoscale
	ax3.draw_artist(line3)


	line4.set_ydata(m)
	line4.set_xdata(n) 
	ax4.relim()        # Recalculate limits
	ax4.autoscale_view(True,True,True) #Autoscale   
	ax4.draw_artist(line4)


	line5.set_ydata(m)
	line5.set_xdata(n)   
	ax5.relim()        # Recalculate limits
	ax5.autoscale_view(True,True,True) #Autoscale 
	ax5.draw_artist(line5)


	line6.set_ydata(m)
	line6.set_xdata(n)  
	ax6.relim()        # Recalculate limits
	ax6.autoscale_view(True,True,True) #Autoscale  
	ax6.draw_artist(line6)


	fig.canvas.blit(ax1.bbox)
	fig.canvas.blit(ax2.bbox)
	fig.canvas.blit(ax3.bbox)
	fig.canvas.blit(ax4.bbox)
	fig.canvas.blit(ax5.bbox)
	fig.canvas.blit(ax6.bbox)
	plt.pause(0.00000001)


print 'FPS:' , 6000/(time.time()-tstart)
