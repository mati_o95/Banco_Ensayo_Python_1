import matplotlib.pyplot as plt
import numpy as np
import time

t = [0,1,2,3,4,5,6,7,8,9]

u = [2,3,4,8,9,3,2,5,4,5]
v = [1,5,3,4,7,9,3,4,8,6]
w = [0,4,6,8,7,9,3,5,6,8]
x = [0,4,2,8,9,3,5,6,7,8]
y = [0,2,4,8,6,9,7,3,2,5]
z = [0,4,5,3,7,8,9,3,1,8]
d = [u,v,w,x,y,z]
l = [0,0,0,0,0,0,0,0,0,0]
next = [5,7,9,9,9,6,4,8,5,2]

fig, axes = plt.subplots(nrows=6)

fig.show()

# We need to draw the canvas before we start animating...
fig.canvas.draw()

styles = ['r-', 'g-', 'y-', 'm-', 'k-', 'c-']
def plot(ax, style):
    return ax.plot(t, l, style, animated=True)[0]
lines = [plot(ax, style) for ax, style in zip(axes, styles)]

# Let's capture the background of the figure
backgrounds = [fig.canvas.copy_from_bbox(ax.bbox) for ax in axes]

tstart = time.time()
for i in xrange(1, 1000):
    items = enumerate(zip(lines, axes, backgrounds), start=1)
    for j, (line, ax, background) in items:
        fig.canvas.restore_region(background)
        #line.set_ydata(np.sin(j*x + i/10.0))
	line.set_ydata((d[j][1]:)+next.pop())# + 10.0)
	print i
        ax.draw_artist(line)
        fig.canvas.blit(ax.bbox)
##INCOMPLETO EL CODIGO


print 'FPS:' , 2000/(time.time()-tstart)
