import matplotlib.pyplot as plt
import numpy as np
import time 

#x = np.arange(0, 2*np.pi, 0.01)
#y = np.sin(x)
#y = x


x= [0,1,2,3,4,5,6,7,8,9]
a=[10,11,12,13,14,15,16,17,18,19]
b=[20,21,22,23,24,25,26,27,28,29]
c=[30,31,32,33,34,35,36,37,38,39]
d=[40,41,42,43,44,45,46,47,48,49]
t=[a,b,c,d]
y=[5,7,8,9,6,4,7,4,6,9]

u=[8,9,7,6,6,3,4,8,2,5]
v=[1,1,3,7,20,6,30,4,5,4]
w=[2,4,3,5,3,6,3,8,9,4]
z=[10,14,15,16,2,3,48,52,3,2]

l=[u,v,w,z]

fig, (ax1,ax2,ax3,ax4,ax5,ax6) = plt.subplots(nrows=6)
fig.show()
ax1.set_autoscale_on(True) # enable autoscale
ax1.autoscale_view(True,True,True)
ax2.set_autoscale_on(True) # enable autoscale
ax2.autoscale_view(True,True,True)
ax3.set_autoscale_on(True) # enable autoscale
ax3.autoscale_view(True,True,True)
ax4.set_autoscale_on(True) # enable autoscale
ax4.autoscale_view(True,True,True)
ax5.set_autoscale_on(True) # enable autoscale
ax5.autoscale_view(True,True,True)
ax6.set_autoscale_on(True) # enable autoscale
ax6.autoscale_view(True,True,True)

fig.canvas.draw()

line = ax1.plot(x, y, 'b-', animated=True)[0]
line2 = ax2.plot(x, y, 'r-', animated=True)[0]
line3= ax3.plot(x, y, 'g-', animated=True)[0]
line4= ax4.plot(x, y, 'c-', animated=True)[0]
line5= ax5.plot(x, y, 'm-', animated=True)[0]
line6= ax6.plot(x, y, 'y-', animated=True)[0]

background1 = fig.canvas.copy_from_bbox(ax1.bbox)
background2 = fig.canvas.copy_from_bbox(ax2.bbox)
background3 = fig.canvas.copy_from_bbox(ax3.bbox)
background4 = fig.canvas.copy_from_bbox(ax4.bbox)
background5 = fig.canvas.copy_from_bbox(ax5.bbox)
background6 = fig.canvas.copy_from_bbox(ax6.bbox)

tstart = time.time()
for j in range(0,4):
	for i in range(0, 9):
		fig.canvas.restore_region(background1)
		fig.canvas.restore_region(background2)
		fig.canvas.restore_region(background3)
		fig.canvas.restore_region(background4)
		fig.canvas.restore_region(background5)
		fig.canvas.restore_region(background6)
		#	line.set_ydata(l[j][i])
		#line.set_xdata(t[j][i])
		ax1.relim()        # Recalculate limits
		ax1.autoscale_view(True,True,True) #Autoscale
		ax1.draw_artist(line)
		#line2.set_ydata(2*x+i)
		#line2.set_xdata(t[i])
		ax2.relim()        # Recalculate limits
		ax2.autoscale_view(True,True,True) #Autoscale
		ax2.draw_artist(line2)
		#line3.set_ydata(3*x+i)
		#line3.set_xdata(t[i])
		ax3.relim()        # Recalculate limits
		ax3.autoscale_view(True,True,True) #Autoscale
		ax3.draw_artist(line3)
		#line4.set_ydata(4*x+i)
		#line4.set_xdata(t[i])
		ax4.relim()        # Recalculate limits
		ax4.autoscale_view(True,True,True) #Autoscale
		ax4.draw_artist(line4)
		#line5.set_ydata(5*x+i)
		#line5.set_xdata(t[i])
		ax5.relim()        # Recalculate limits
		ax5.autoscale_view(True,True,True) #Autoscale
		ax5.draw_artist(line5)
		#line6.set_ydata(6*x+i)
		#line6.set_xdata(t[i])
		ax6.relim()        # Recalculate limits
		ax6.autoscale_view(True,True,True) #Autoscale
		ax6.draw_artist(line6)
		fig.canvas.blit(ax1.bbox)
		fig.canvas.blit(ax2.bbox)
		fig.canvas.blit(ax3.bbox)
		fig.canvas.blit(ax4.bbox)
		fig.canvas.blit(ax5.bbox)
		fig.canvas.blit(ax6.bbox)
		plt.pause(0.5)
print 'FPS:' , 4/(time.time()-tstart)

