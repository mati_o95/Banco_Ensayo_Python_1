import matplotlib.pyplot as plt
import numpy as np
import time 

x = np.arange(0, 2*np.pi, 0.01)
y = np.sin(x)

fig, (ax1,ax2,ax3,ax4,ax5,ax6) = plt.subplots(nrows=6)
fig.show()

fig.canvas.draw()

line = ax1.plot(x, y, 'b-', animated=True)[0]
line2 = ax2.plot(x, y, 'r-', animated=True)[0]
line3= ax3.plot(x, y, 'g-', animated=True)[0]
line4= ax4.plot(x, y, 'c-', animated=True)[0]
line5= ax5.plot(x, y, 'm-', animated=True)[0]
line6= ax6.plot(x, y, 'y-', animated=True)[0]

background1 = fig.canvas.copy_from_bbox(ax1.bbox)
background2 = fig.canvas.copy_from_bbox(ax2.bbox)
background3 = fig.canvas.copy_from_bbox(ax3.bbox)
background4 = fig.canvas.copy_from_bbox(ax4.bbox)
background5 = fig.canvas.copy_from_bbox(ax5.bbox)
background6 = fig.canvas.copy_from_bbox(ax6.bbox)

tstart = time.time()
for i in range(1, 2000):
    fig.canvas.restore_region(background1)
    fig.canvas.restore_region(background2)
    fig.canvas.restore_region(background3)
    fig.canvas.restore_region(background4)
    fig.canvas.restore_region(background5)
    fig.canvas.restore_region(background6)
    line.set_ydata(np.sin(x + i/10.0))
    ax1.draw_artist(line)
    line2.set_ydata(np.sin(2*x + i/10.0))
    ax2.draw_artist(line2)
    line3.set_ydata(np.sin(3*x + i/10.0))
    ax3.draw_artist(line3)
    line4.set_ydata(np.sin(4*x + i/10.0))
    ax4.draw_artist(line4)
    line5.set_ydata(np.sin(5*x + i/10.0))
    ax5.draw_artist(line5)
    line6.set_ydata(np.sin(6*x + i/10.0))
    ax6.draw_artist(line6)
    fig.canvas.blit(ax1.bbox)
    fig.canvas.blit(ax2.bbox)
    fig.canvas.blit(ax3.bbox)
    fig.canvas.blit(ax4.bbox)
    fig.canvas.blit(ax5.bbox)
    fig.canvas.blit(ax6.bbox)

print 'FPS:' , 2000/(time.time()-tstart)

