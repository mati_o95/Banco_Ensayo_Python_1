#!/usr/bin/env python
import numpy as np
import matplotlib.mlab as mlab
import matplotlib.pyplot as plt
import xlrd
import sys
import matplotlib.animation as animation
import time

#argv1:nombre de archivo fuente
#argv2: primer fila
#argv3: columna a leer


data = []
tiempo = []
i=0

#Creamos Figura y Subfiguras
fig = plt.figure('Banco de Ensayo',figsize=(10, 10))
#fig.add_subplot(2,2,1)
#fig.add_subplot(2,2,2)
#fig.add_subplot(2,2,3)
#fig.add_subplot(2,2,4)

book = xlrd.open_workbook(sys.argv[1])
sh = book.sheet_by_index(0)

for i in range(sh.nrows-int(sys.argv[2])):
    data.append(sh.cell_value(i+int(sys.argv[2]), int(sys.argv[3])))
    tiempo.append(sh.cell_value(i+int(sys.argv[2]),1))

tiempo_min=tiempo_max=tiempo[0]
for i in tiempo:
		if i<tiempo_min:
			tiempo_min = i
		if i>tiempo_max:
			tiempo_max = i

plt.plot(tiempo, data, 'r--', linewidth=1.5,)
plt.xlim(np.min(tiempo),np.max(tiempo))
plt.ylabel(sh.cell_value(int(sys.argv[2])-1, int(sys.argv[3])))
plt.xlabel("Tiempo")
plt.title('Analisis de Ensayo')


plt.show()

