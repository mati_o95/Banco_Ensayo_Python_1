#!/usr/bin/env python
import numpy as np
import matplotlib.mlab as mlab
import matplotlib.pyplot as plt
import xlrd
import sys

# mu es la media y sigma es la desviacion tipica
# create an histogram from .xls file
#
#argv1:nombre de archivo fuente
#argv2: primer fila
#argv3: columna a leer
data = []
i=0

book = xlrd.open_workbook(sys.argv[1])
sh = book.sheet_by_index(0)

for i in range(sh.nrows-int(sys.argv[2])):
    data.append(sh.cell_value(i+int(sys.argv[2]), int(sys.argv[3])))

mu= np.mean(data)
sigma= np.std(data, 0)
print('Media= %f  Desviacion Tipica= %f\n' %(mu,sigma))

# the histogram of the data with histtype='step'
n, bins, patches = plt.hist(data, 50, normed=1, histtype='stepfilled', facecolor='red', alpha=0.5)

# add a line showing the expected distribution
y = mlab.normpdf( bins, mu, sigma)
plt.plot(bins, y, 'k--', linewidth=1.5)
plt.title('Media= %f  Desviacion Tipica= %f'%(mu,sigma))


plt.show()

